import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

//B.1.2.1 document.querySelectorAll("nav ul a")
//B.1.2.2 document.querySelectorAll(".pizzaList li")
//B.2.1.1 document.querySelectorAll(".pizzaThumbnail h4")[1].innerHTML
//B.2.1.2
document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";
//B.2.2.1 document.querySelectorAll("footer a")[1].getAttribute("href")
//B.2.2.2 document.querySelectorAll("a")[1].getAttribute("class")
document.querySelectorAll('a')[1].setAttribute('class', 'pizzaListLink active');

//C.2.1
document.querySelector('.newsContainer').setAttribute('style', '');
//document.querySelector('.newsContainer').style = '';
//document.querySelector('.newsContainer').removeAttribute('style');
//C.2.2

document.querySelector('.closeButton').addEventListener('click', event => {
	event.preventDefault();
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});
